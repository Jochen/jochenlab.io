---
title: My blog is live
date: 25/05/2020
tags: web
category: news
---

## Finally I have a website

After much procacrastination I finally managed to create my own website and blog. I've tried several times
to finally start, but I always get stuck researching the best website engine, looking for great themes and 
customising the selected theme half way and giving up for one reason or the other.  I will write a bit about 
how I chose the site generator in a separate [post]({filename}staticgenerators.md).
