---
title: A comparison of static site generators
date: 26/05/2020 10:00
tags: pelican, blog, gatsby, web, jekyll, hugo
category: review
---


## How to create your own academic website

I'm an academic working at Chalmers University of Technology. So my requirements are somewhat different 
to usual bloggers and software developers, however not really unusual. 

I want an open source solution or at least a solution were I have easy access to my data, if I ever 
decide to move to a different platform. In particular, I like to write my posts in either `markdown`, `reST`
or `orgmode`. 

While I briefly looked at solutions like [ghost](https//ghost.org) or single file CMS's like [grav](https://grav.org)
(which really looks awesome), I quickly decided on a static website generator. Most static generators, take 
`markdown` or similar formats and convert to static html documents. Those can then be easily hosted using 
[github](https://www.github.com) or [gitlab](https://www.gitlab.com) pages. There are a ton of static web generators
out there, I get the impression that for a while it was a favourite past-time to write a static site generators 
using your language of choice. When narrowing down my search I was particularly looking for the following requirements:

Flexible theming/nice themes
::  I'm not particularly good at design, but I do appreciate a good looking website, I do believe that it makes
    a difference for visitors how your site looks. In particular if it is readable on mobile devices. So there should
    be a flexible way of adjusting the look and feel of the websites. If there are attractive themes that can be easily
    adjusted to my needs than this is a bonus.

Publication list/Bibtex support
:   As academics the main thing we do is publish papers. Part of the reason for making this website is to create
    a portfolio, which must include a publication list. I essentially keep my publication lists in a large bibtex 
    file (I use [zotero](https://www.zotero.org) in fact, but only export to bibtex), so I want a way to automatically
    generate the list from a supplied bibtex file. I also want the ability to cite documents from blog posts. 

Jupyter notebook support
:   I'm a big python fan and I already have plans for several posts about scientific programming. It would 
    be possible to do this by exporting figures and including them in .e.g. markdown posts, or 
    alternatively manually converting the notebooks to html. However, knowing myself I would probably not
    get around to doing this. 

After some research I narrowed my search down to three choices:

1. [Hugo](https://gohugo.io) and the [Academic theme](https://themes.gohugo.io/academic) in particular
2. [Jekyll]( https://jekyllrb.com )
3. [ GatsbyJS ]( https://gatsbyjs.org )
4. [ Pelican ]( https://getpelican.com )

In the following I will discuss some of the pros and cons I found with each of them.

### Hugo

[ Hugo ][https://gohugo.io] is written in golang and while still relatively young, has a huge community build around it. One great thing about 
the project is that there is essentially a single static file that you can download, no dependency
tracking, no building from source. It is also blazingly fast, in the couple of tests I ran it was really
impressive how fast it was. Because there is such a large community, there are a lot of great looking themes. 
However, I did not quite understand the ability to use plugins. It seems more that hugo uses pre or post processors
written in other languages. If you suddenly have install e.g. `node` it somewhat removes the advantage of the 
single static binary. Also looking at themes and integration with publications and jupyter notebooks, it seems
one should choose [ Hugo Academic ]( https://themes.gohugo.io/academic ), which has all that functionality build in.
However when I was reading some more, on how to adjust the academic theme to my liking, I stumbled across
quite a few posts, discussing how difficult this is. 

#### Pros

- fast!
- large community
- very nice themes available
- academic theme has jupyter and publication support build in

#### Cons

- relies on external tools for e.g. notebook conversion. Not an issue per say, but seems to make some integration tricky.
- for a full "academic experience" should go with the academic theme
- academic theme is allegedly not easy to customize


#### Verdict

I initially wanted to use hugo, but because I was not a big fan of the default look of the academic theme and reading about the 
difficulties to adjust the theme I decided to look at some of the other alternatives. If you want a quick solution that comes with
most things out of the box, I think hugo is probably the best choice.

### Jekyll

[ Jekyll ]( https://jekyllrb.com ) is probably one of the oldest static site generators around and has been arguably 
one of the driving forces behind the popularity of static websites. It is written in ruby and
tightly integrated with github pages which makes publishing with it very easy.  Due to its age and popularity
there is a huge community who have created some amazing templates and plugins. For acadmic purposes there is the
the [scholar](https://github.com/inukshuk/jekyll-scholar) plugin which allows you to include bibtex citations and publication lists.
There is also a [plugin](https://github.com/red-data-tools/jekyll-jupyter-notebook) to render jupyter notebooks. 
Finally, there are a number of good looking themes aimed at academic websites, I particularly liked the [academicpages](https://academicpages.github.io/) 
and [al-folio](https://github.com/alshedivat/al-folio) themes. The first thing you notice is that for jekyll to work you
have to install a lot of ruby modules, in particular when using the academic themes. This is relatively straight forward using ruby's bundler,
however considering I was not familiar with ruby it took me a while to figure out how to install things. Short answer don't install system white, just 
use bundler to install and run things in your website directory. When running it you quickly notice that jekyll is significantly slower than hugo. I also
encountered a couple of small bugs (or user error) with some packages. To resolve these I needed to look at the source code, which was a bit harder 
considering I haven't used ruby before.

#### Pros

- huge community
- lots of plugins
- easy integration with github-pages (although with limited plugin support)
- very nice themes available

#### Cons
- slow (compared to hugo)
- some packages seem not well maintained
- I'm not familiar with ruby and some customisation does require looking into the code

#### Verdict

In the end I decided not to go with Jekyll, mainly because it seemed necessary that if you want to make advanced customisations it would be good to know ruby. I 
just don't have the bandwidth for learning ruby ATM.

### GatsbyJS

[GatsbyJS](https://gatsbyjs.org) is a little different because it is more like a framework to build webapplications based on react which can then be compiled to static websites.
It is the new kid on the block, but there is already a huge community and it definitely seems like the new big thing. There is some excellent documentation around using gatsbyjs 
and building your own website as well as some amazing looking starters. There are also lots of plugins, in particular a plugin for integrating jupyter notebooks as well as a bibtex plugin.
I was actually quite excited when I found gatsby, because I've been meaning to learn javascript and more web technology so this seemed like a great opportunity. 

Building a  website with gatsby is quite different to the other static site generators, instead of making your templates using jinja or mako that will then be filled in the processing,
you instead connect different components. It feels much more like GUI programming. That said, at the moment the project seem much more aimed at webdevs and there is not much in terms of 
tutorials or starters for building academic websites.

However, after you going through the tutorials and when I was starting to build my own website I came across a couple of bugs, the biggest in the jupyter notebook plugin, which was not working
for me and seems to be unmaintained (there was an open bug with the same issue already). 

#### Pros
- huge community
- extremely powerful
- great documentation
- many great looking themes and starters

#### Cons
- a bit of a learning curve if you are not familiar with JS and react 
- requires learning JS and react, even when taking an existing starter, you need to learn the technology to make adjustments

#### Verdict

After discovering gatsby I really wanted to use it. In the end the bug in the jupyter notebook plugin was the deal-breaker for me. I did try to resolve it, but could not quickly figure out the problem,
so decided to use something else to get something up and running. 

### Pelican

[Pelican](https://getpelican.com) is a static website generator written in Python. Considering I do most of my programming in Python this already has a big appeal for me. It is not as big as the
the other generators mentioned, but does have quite an active community and a significant number of plugins, including plugins for bibtex and jupyter notebooks. Compared to hugo, jekyll and gatsbyjs, the
community seems to much less webdevelopers and more technical people from other backgrounds, which IMO shows in the themes with many looking a bit dated. 

Posts and pages can be written in `markdown`, `ReStructuredText` by default, but there is also a plugin for `org-mode` documents, which is good for me, because I do my notetaking using org. 

#### Pros
- written in Python
- many plugins
- very good documentation

#### Cons
- most themes not that exciting
- community not as large as the others

#### Verdict

In the end I decided to use Pelican. The biggest deciding factor was that it is written in Python. I could already write some small plugins just because I'm familiar with the language. For the theme
I ended up adjusting the al-folio theme for jekyll, which I will put up on github eventually.
