---
title: Hosting your Pelican site on gitlab pages
date: 26/05/2020 13:00
tags: pelican, blog, web, gitlab, ci
category: review
---

Hosting a static website on gitlab pages is very convenient. However, if you have not used continuous integration (CI or gitlab runners) before
like myself you might run into a couple of issues I'm documenting some of the problems here and give some fixes.

### Requirements.txt

Make sure that you have included all the required packages in a requirements.txt file, so that the runner image can install all dependencies. 

### Docker image

My website depends on the pelican `assets plugin` which depends on the `webassets` module and the `libsass` python bindings. 
The gitlab pelican [example repository](https://gitlab.com/pages/pelican) has an example ci configuration which uses an alpine linux based
image. Because the libsass bindings are a C-extension and there is no binary wheel for alpine, pip needs to compile the bindings, which requires `gcc`.
I was running into some other dependency issues (Alpine is pretty minimal) so it was easier to just move to a different docker image. 
Simply change the `image: python:3.7-alpine` to `image: python:3.7` which uses debian. Note that you need to change install commands
from using `apk` to `apt` as well now. 


### Submodules

I'm including my theme and some plugins as submodules. When building the runner needs to know to also install these. You need to add the 
following section to you `.gitlab-ci.yml` file:

    :::
    variables:
      GIT_SUBMODULE_STRATEGY: recursive

This instructs the runner to checkout all submodules recursively. **Important**: make sure to include submodules using https, if you include with
ssh the runner will fail to checkout the modules (you could probably get around this with some permissions adjustments). 
