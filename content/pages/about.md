title: Jochen Schröder 
author: Jochen Schröder
modified: 2020-05-23
url: /
Template: about
profile: True
news: true
align: right
image: images/me.jpg
save_as: index.html
social: true
affiliation: Photonics Laboratory, <a href="https://www.chalmers.se/en/departments/mc2">Department of Microtechnology and Nanoscience</a>, <a href="https://www.chalmers.se/en">Chalmers University of Technology</a>


I am a tenured senior researcher (research focused associate professor) in the 
photonics laboratory at the Department for Microtechnology and Nanoscience at Chalmers University of Technology
in Gothenburg, Sweden. 

My main research interests are in high-spectral efficiency optical communications, optical space communications as well as 
integrated nonlinear photonics. 

I am a big fan of Python and open source software in general. Together with Nicolas Fontaine from Bell Labs and Binbin Guan 
from Acacia we therefore created the so-called lab-automation hackathons which we have been running at OFC and ECOC.

In my free time if I'm not playing with my two daughters I like to race road bikes and dance cuban salsa.

