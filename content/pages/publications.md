title: Publications
author: Jochen Schröder
date: 2016-06-08

Feel free to contact me if you would like a pdf copy of any of my papers.


## Journal Articles
<script src="https://bibbase.org/show?bib=https://jochenschroeder.com/bib/MyJournals.bib&jsonp=1"></script>

## Conference Contributions
<script src="https://bibbase.org/show?bib=https://jochenschroeder.com/bib/MyConferences.bib&jsonp=1"></script>
