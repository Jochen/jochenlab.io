---
title: Code
author: Jochen Schröder
modified: 2020-05-23
url: /code
save_as: code.html
---


### Open Source Projects

<a href="https://github.com/chalmersphotonicsLab/qampy">QAMPy</a>
:  A DSP toolchain for optical communication systems written in Python. The code is highly optimised for speed using compiled code
  for performance critical components. Released under GPLv3.

<a href="https://gitlab.com/Jochen/jochen.gitlab.io">This page</a> Source code
for this webpage in pelican.

### Education
<a href="https://python4photonics.org">Python4photonics</a>
: Website with resources about using python for photonics research. In particular you can find the notebooks we use for our OFC short
  course in our [gitlab repository](https://gitlab.com/python4photonics/LabAutomationHackathons).

### Other

  <a href="https://gitlab.com/Jochen/jochen.gitlab.io">This page</a>: Repository for this blog which uses
  pelican.
