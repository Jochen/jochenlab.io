# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
MARKUP = ("md",'ipynb' )
from  pelican_jupyter import markup as nb_markup

AUTHOR = 'Jochen Schröder'
SITENAME = 'Jochen Schröder'
SITESUBTITLE = "Python and Photonics"
SITEURL = 'https://jochenschroeder.com'
#SITEURL = 'http://localhost:8000'

MARKDOWN = {
    'extension_configs': {
   #    # https://pythonhosted.org/Markdown/extensions/index.html#officially-supported-extensions
        'markdown.extensions.codehilite': {'css_class': 'highlight'},
        'markdown.extensions.meta': {},
        'markdown.extensions.sane_lists': {},
        'markdown.extensions.smarty': {},
        #'markdown.extensions.toc': {'permalink': True, 'marker': "[TOC]"},
        'markdown.extensions.def_list':{},
   #     # https://facelessuser.github.io/pymdown-extensions/
        #'pymdownx.extra': {},
        #'pymdownx.caret': {'superscript': True},
        #'pymdownx.magiclink': {},
        #'pymdownx.smartsymbols': {},
   #     #'pymdownx.arithmatex': {},
    },
    'output_format': 'html5',
}
TYPOGRIFY = True
PATH = 'content'
TIMEZONE = 'Europe/Stockholm'

DEFAULT_LANG = 'en'
# Do not publish articles set in the future
ARTICLE_URL = "blog/articles/{slug}/"
ARTICLE_SAVE_AS = ARTICLE_URL + 'index.html'
ARTICLE_PATHS = ['blog']
PAGE_URL = '{slug}/'
PAGE_SAVE_AS = PAGE_URL + 'index.html'
PAGE_PATHS = ['pages']
CATEGORY_SAVE_AS = "blog/category/{slug}.html"
CATEGORY_URL = "blog/category/{slug}.html"
CATEGORIES_SAVE_AS = "blog/categories.html"
TAG_SAVE_AS = "blog/tag/{slug}.html"
TAG_URL = "blog/tag/{slug}.html"
TAGS_SAVE_AS = "blog/tags.html"
# I am single author so dont need to save author list
AUTHORS_SAVE_AS = ""
AUTHOR_SAVE_AS = ""

ARCHIVES_SAVE_AS = "blog/archives.html"
#ARTICLE_URL = 'blog/{date:%Y}/{date:%m}/{slug}/'
#ARTICLE_SAVE_AS = ARTICLE_URL + 'index.html'

MENUITEMS = (
    ("Blog", '/blog'),
             ("Publications", "/publications/"),
             ("Code", "/code"),
             #("Research", "/research"),
             ("About", "/")
             )
DISPLAY_PAGES_ON_MENU = False
# I want to set the home page to something else
INDEX_SAVE_AS = "blog/index.html"
INDEX_URL = "blog/"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None


# Social widgets are being used by the about page look at the social template for a 
# possible values
SOCIAL = (("email", "jochen.schroeder@chalmers.se"),
          ("github_username", "cycomanic"),
          ("scholar_id", "5F_2ItoAAAAJ"),
          ('orcid_id', "0000-0002-1016-8152"),
          ("twitter_username", "DrJochenSchroeder" ),
          ("strava_userid", "150547"))

DEFAULT_PAGINATION = 5
# Note to put under the social media buttons
CONTACT_NOTE = "Best to send me an email"

PLUGIN_PATHS= ['plugins']
# Allow to post notebooks and precompile latex using katex
PLUGINS = ["liquid_tags", nb_markup, 'assets', 'pelican-latex-prerender']
# Notebook checkpoints should be ignored
IGNORE_FILES = [".ipynb_checkpoints"]

# Blog name and description are being used by the blog index page, so the blog can have a different name than the website
BLOG_NAME = "Research, Photonics and Python"
BLOG_DESCRIPTION = "Thoughts on research, open-science, python and photonics"

# text to put in the footer
FOOTER_TEXT = """Copyright Jochen Schröder. Powered by <a href="http://getpelican.com/">Pelican</a>, Theme based on <a href="https://github.com/alshedivat/al-folio">al-folio for jekyll</a>"""
THEME = "theme/"
#THEME = "../Themes/m.css/pelican-theme"

# fix for pelican-jupyter because of nbconvert moving to 6.0 see https://github.com/danielfrg/pelican-jupyter/issues/126
IPYNB_EXPORT_TEMPLATE =  "base" 
IPYNB_SKIP_CSS = True
IPYNB_COLORSCHEME = "default.css"
STATIC_PATHS = ['images', 'bib']
# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
